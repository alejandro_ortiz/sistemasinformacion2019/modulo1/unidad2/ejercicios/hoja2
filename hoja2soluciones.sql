﻿USE ciclistas;

-- 1. Numero de ciclistas que hay

  SELECT 
    COUNT(*)num_ciclistas 
  FROM 
    ciclista c;

-- 2. Numero de ciclistas que hay en el equipo Banesto

  SELECT 
    COUNT(*)ciclistas_banesto 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto';

-- 3. La edad media de los ciclistas

  SELECT 
    AVG(c.edad)edad_media 
  FROM 
    ciclista c;

-- 4. La edad media de los de equipo Banesto

  SELECT 
    AVG(edad)edad_media_banesto 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto';

-- 5. La edad media de los ciclistas por cada equipo

  SELECT 
    c.nomequipo, AVG(edad)edad_media 
  FROM 
    ciclista c 
  GROUP BY c.nomequipo;

-- 6. El número de ciclistas por equipo 

  SELECT 
    c.nomequipo, COUNT(*)num_ciclista 
  FROM 
    ciclista c 
  GROUP BY c.nomequipo;

-- 7. El número total de puertos

  SELECT 
    COUNT(*)num_puertos 
  FROM 
    puerto p;

-- 8. El número total de puertos mayores de 1500

  SELECT 
    COUNT(*)puertos_1500m 
  FROM 
    puerto p 
  WHERE 
    p.altura>1500;

-- 9. Listar el nombre de los equipos que tengan más de 4 ciclistas

  -- ciclistas por equipo (c1)
  SELECT 
    c.nomequipo, COUNT(*)num_ciclistas 
  FROM 
    ciclista c 
  GROUP BY c.nomequipo;

  -- completa
  SELECT 
    c1.nomequipo 
  FROM (
    SELECT 
      c.nomequipo, COUNT(*)num_ciclistas 
    FROM 
      ciclista c 
    GROUP BY c.nomequipo
    ) c1
  WHERE 
    c1.num_ciclistas>4;

  -- con HAVING
  SELECT 
    c.nomequipo
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo 
  HAVING 
    COUNT(*)>4;

-- 10. Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32

 -- ciclistas por equipo (c1)
  SELECT 
    c.nomequipo, COUNT(*)num_ciclistas 
  FROM 
    ciclista c 
  WHERE 
    c.edad BETWEEN 28 AND 32
  GROUP BY c.nomequipo;

  -- completa
  SELECT 
    c1.nomequipo 
  FROM (
    SELECT 
      c.nomequipo, COUNT(*)num_ciclistas 
    FROM 
      ciclista c 
    WHERE 
      c.edad BETWEEN 28 AND 32
    GROUP BY c.nomequipo
    ) c1
  WHERE 
    c1.num_ciclistas>4;

-- con HAVING
  SELECT 
    c.nomequipo
  FROM 
    ciclista c
  WHERE 
    c.edad BETWEEN 28 AND 32 
  GROUP BY 
    c.nomequipo 
  HAVING 
    COUNT(*)>4;

-- 11. Indícame el número de etapas que ha ganado cada uno de los ciclistas

  SELECT 
    e.dorsal, COUNT(*)num_etapas 
  FROM 
    etapa e 
  GROUP BY e.dorsal;
  
-- 12. Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa

-- victorias de cada ciclista (c1)
  SELECT 
    e.dorsal, COUNT(*)num_etapas 
  FROM 
    etapa e 
  GROUP BY e.dorsal;

  -- completa
  SELECT 
    c1.dorsal 
  FROM (
    SELECT 
      e.dorsal, COUNT(*)num_etapas 
    FROM 
      etapa e 
    GROUP BY e.dorsal
    ) c1
  WHERE 
    c1.num_etapas>1;

-- con HAVING
  SELECT 
    e.dorsal
  FROM 
    etapa e
  GROUP BY 
    e.dorsal 
  HAVING 
    COUNT(*)>1;

